Curious and self-taught by nature, my history with computing began in the late
2000s, tinkering with the family PC (sometimes with disastrous results). This
led me to always try to answer the "how does it work...?", "how can I do...?",
or "how can I fix...?", leading me to self-investigation and
exploration in various mediums that provide me with the knowledge to
**fix**, **create**, and **build** projects.

The area I like the most is **web programming** as a **fullstack
developer**, but it is not the only thing I can do, as I'm also good in creating
**terminal applications** and **scripts**. I can work to solve any problem in a
conventional way or invent new solutions.

My love for video games has played an important role in my life, not only in
enjoying playing them, but going beyond to understand how they work
under the hood in their different areas: game mechanics, narratives, level
design, music, art, among others. I find it fascinating to see how developers
overcome difficulties and invent innovative solutions to provide the best
experience for the players.

My philosophy is based on **always trying to improve**, though studies,
research and updates that have provided me with knowledge of various
technologies, such as: Javascript, Python, Rust, Lua, React, SASS, Bootstrap,
Flask, ExpressJS, SQL, NoSQL, among others. Because of this, I am always in an
infinite cycle of improvement and learning.

My love for computer science has led me to explore and investigate other areas
and interests, of which I can mention:

- Being an enthusiast of **Linux** operating systems, using it both on my personal computers and on servers.
- Special interest in maintaining my development environment **personalized and updated**, allowing me to develop in a comfortable, ergonomic and effective way.
- Being part of the **selfhost** community, managing a variety of applications for personal use on **small personal servers**.
- Being **passionate about video games**.
