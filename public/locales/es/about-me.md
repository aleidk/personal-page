Curioso y autodidacta por naturaleza, mi historia con la computación parte a
finales de los 2000, jugueteando con la PC familiar (a veces con resultados
desastrosos). Esto me llevó a siempre tratar de responder el _"¿cómo
funciona…?"_, _"¿cómo puedo hacer…?"_, o _¿cómo puedo solucionar…?_, llevándome
a la auto-investigación y exploración en diversos medios que me entreguen el
conocimiento para poder **resolver**, **crear** y **construir** proyectos.

Lo que más me apasiona es la **programación web** como un **desarrollador
fullstack**, pero no es lo único que puedo realizar, ya que también me
desenvuelvo bien en la creación de **aplicaciones de terminal** y **scripts**.
Puedo trabajar buscando resolver cualquier problema de forma convencional o
inventar soluciones nuevas.

Mi amor por los videojuegos ha jugado un rol importante en mi, lo cuál no se
refiere tan solo disfrutar jugarlos, sino ir más allá, para lograr ver como
funcionan por dentro en sus distintas disciplinas: mecánicas de juego,
narrativas, diseño de niveles, música, arte, entre otros. Encuentro fascinate
ver como los desarrolladores logran sortear dificultades he inventar soluciones
innovadoras para lograr dar la mejor experiencia al jugador.

Mi filosofía se basa en **siempre intentar mejorar**, realizando
estudios, investigaciones y actualizaciones que me han proporcionado el
conocimiento de diversas tecnologías, tales como: Javascript, Python, Rust,
Lua, React, SASS, Bootstrap, Flask, ExpressJS, SQL, NoSQL, entre
otros. Debido a esto siempre estoy en un infinito ciclo de mejora y
aprendizaje.

Mi amor por la informática me ha llevado a extender he investigar otras areas e
intereses, de los cuales puedo nombrar:

- Ser un entusiasta de los sistemas operativos **linux**, usándolo tanto en mis computadores personales como en servidores.
- Interés especial en mantener mi **ambiente de desarrollo personalizado y actualizado**, permitiendome desarrollar de una manera cómoda, ergonómica y eficaz.
- Ser parte de la comunidad **selfhost**, administrando varias aplicaciones para uso personal en **pequeños servidores propios**.
- Ser **apasionado de los videojuegos**.
