module.exports = {
  tabWidth: 2,
  singleQuote: true,
  endOfLine: 'auto',
  'eol-last': 2,
  trailingComma: 'all',
  plugins: [require.resolve("prettier-plugin-astro")],
  overrides: [
    {
      files: "*.astro",
      options: {
        parser: "astro",
      },
    },
  ],
};
