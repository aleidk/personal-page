import { defineConfig } from 'astro/config';

import react from '@astrojs/react';
import astroI18next from 'astro-i18next';

// https://astro.build/config
export default defineConfig({
  prefetch: true,
  integrations: [react(), astroI18next()],
  experimental: {},
  redirects: {
    '/projects': '/projects/1',
  },
});
