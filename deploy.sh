#!/bin/env bash

pnpm run build

## Add domain to codeberg pages
cat >dist/.domains <<EOL
blog.panconpalta.win
personal-page.aleidk.codeberg.page
pages.personal-page.aleidk.codeberg.page
EOL

pnpm exec gh-pages --dist dist --branch pages --dotfiles
