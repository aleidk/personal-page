---
title: Destino Temuco
status: Backlog
draft: true
created_at: 2024-01-01
updated_at: 2024-01-21
brief: "La dirección de Turismo, Patrimonio y Cultura del Municipio de Temuco, solicito una web autoadministrable, el cual pueda servir a todo turista y publico en general como una guia informativa sobre los servicios turísticos que ofrecen en Temuco. La web permite a quien la visita, saber sobre los tours, atractivos turísticos, eventos, galería de artes, biblioteca y servicios turísticos, así como las opciones para poder llegar a Temuco. También cuenta con blog de noticias y una revista digital sobre Temuco."
timeframe: 'March 2022 - October 2023'
thumbnail: './_media/temuco1.webp'
technologies:
  - React
  - NodeJS
  - MySQL
---

This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![](./_media/piloto2.png)
This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![](./_media/piloto3.webp)
This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![](./_media/piloto4.webp)
This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![](./_media/piloto5.webp)
