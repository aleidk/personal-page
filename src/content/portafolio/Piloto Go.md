---
title: Piloto Go
status: Backlog
draft: true
created_at: 2024-01-01
updated_at: 2024-01-21
brief: "Para el área de innovación de Almagro desarrollamos un producto mínimo viable que permite digitalizar las solicitudes de los nuevos productos que se utilizan en la construcción de edificios. Además permite armar digitalmente las matrices para los distintos segmentos de las propiedades."
timeframe: 'March 2022 - October 2023'
thumbnail: './_media/piloto1.webp'
technologies:
  - React
  - NodeJS
  - MySQL
---

This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![img2](./_media/piloto2.png)

This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![img3](./_media/piloto3.webp)

This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![img 4](./_media/piloto4.webp)

This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![](./_media/piloto5.webp)
