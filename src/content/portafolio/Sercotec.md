---
title: Barrios Comerciales - Sercotec
status: Backlog
draft: true
created_at: 2024-01-01
updated_at: 2024-01-21
brief: "Desarrollo de sitio web para SERCOTEC donde buscan visibilizar los barrios comerciales dentro de Chile. Para esto cada uno de los barrios ya mapeados por SERCOTEC se pueden cargar con la descripción, imágenes, videos, programas y un mapa de como llegar al barrio. También es posible ingresar nuevos barrios comerciales que no estén inscritos en el sitio."
timeframe: 'March 2022 - October 2023'
thumbnail: './_media/sercotec1.webp'
technologies:
  - React
  - NodeJS
  - MySQL
---

This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![](./_media/piloto2.png)
This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![](./_media/piloto3.webp)
This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![](./_media/piloto4.webp)
This is a web application to manage the stock of product in a construction company and their usage across projects. It also allows to create budgets with the interaction of different actors.

![](./_media/piloto5.webp)
