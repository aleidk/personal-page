import { defineCollection, z } from 'astro:content';

// AstroJS collection configuration

const blog = defineCollection({
  type: 'content',
  schema: z.object({
    title: z.string(),
    draft: z.boolean().optional(),
    tags: z.array(z.string()).optional(),
    published_at: z.coerce.date().optional(),
    updated_at: z.coerce.date().optional(),
  }),
});

const portafolio = defineCollection({
  type: 'content',
  schema: ({ image }) =>
    z.object({
      title: z.string(),
      draft: z.boolean().optional(),
      brief: z.string(),
      status: z.enum(['Backlog', 'Activo', 'Fixes', 'Finalizado']),
      tags: z.array(z.string()).optional(),
      technologies: z.array(z.string()),
      created_at: z.coerce.date().optional(),
      updated_at: z.coerce.date().optional(),
      thumbnail: image().refine((img) => img.width >= 1080, {
        message: 'Cover image must be at least 1080 pixels wide!',
      }),
      timeframe: z.string().optional(),
      links: z
        .object({
          repo: z.string().url(),
          url: z.string().url(),
        })
        .optional(),
    }),
});

export const collections = {
  blog,
  portafolio,
};
