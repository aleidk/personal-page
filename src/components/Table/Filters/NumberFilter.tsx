import React, { useState, useEffect } from 'react';

interface Props {
  onChange: (value: [string, number | null]) => void;
  keyData: string;
}

export default function NumberInput({ keyData, onChange }: Props): JSX.Element {
  const [value, setValue] = useState<string>('');
  const [operator, setOperator] = useState<string>('=');

  useEffect(() => {
    onChange([operator, value === '' ? null : parseFloat(value)]);
  }, [value, operator]);

  return (
    <div className="hstack">
      <select
        name={`number-select-${keyData}`}
        id={`number-select-${keyData}`}
        defaultValue={'='}
        onChange={({ target }) => {
          setOperator(target.value);
        }}
      >
        <option value="=">=</option>
        <option value=">">{'>'}</option>
        <option value="<">{'<'}</option>
        <option value=">=">{'>='}</option>
        <option value="<=">{'<='}</option>
      </select>
      <input
        name={`number-input-${keyData}`}
        id="foo"
        type="number"
        placeholder="1"
        onChange={({ target }) => {
          setValue(target.value);
        }}
      />
    </div>
  );
}
