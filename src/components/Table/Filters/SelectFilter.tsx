import React, { useMemo } from 'react';
import type { DataItem, Value } from '../types';
import SelectInput from '@components/Inputs/SelectInput';

interface Props {
  onChange: (value: Value) => void;
  data: DataItem[];
  keyData: string;
  isMultiple?: boolean;
}

export default function SelectFilter({
  data,
  keyData,
  isMultiple = false,
  onChange,
}: Props): JSX.Element {
  const options = useMemo(() => {
    let options = [];

    if (isMultiple) {
      options = data.flatMap((item) => item[keyData]);
    } else {
      options = data.map((item) => item[keyData]);
    }

    options = [...new Set(options)];

    options = options.map((item) => ({ label: item, value: item }));

    return options;
  }, [data, keyData]);

  return (
    <SelectInput
      options={options}
      isMultiple={isMultiple}
      onChange={onChange}
    />
  );
}
