export type DataItem = Record<string, any>;

export type Value = string | string[] | number | number[] | null;

export enum HeaderType {
  Index,
  String,
  Number,
  Select,
  Multiple,
}

export interface Header {
  key: string;
  header: string;
  type: HeaderType;
  hasCustomCell?: boolean;
  formatter?: (data: any) => string;
}

export interface Filter {
  type: HeaderType;
  value: Value;
}
