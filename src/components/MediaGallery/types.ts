export enum MediaType {
  Image = 'image',
  Video = 'video',
}

export interface Media {
  type: MediaType;
  url: string;
  alt: string;
  mime?: string;
  thumbnail?: string;
}
