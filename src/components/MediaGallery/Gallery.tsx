import React from 'react';
import { type Media } from './types';

import classes from './Gallery.module.css';
import Carousel from '@components/Carousel/Carousel';
import CarouselItem from '@components/Carousel/CarouselItem';

interface Props {
  items: Media[];
  height: number;
}

export default function Gallery({ items, height = 500 }: Props): JSX.Element {
  return (
    <div style={{ height }}>
      <Carousel>
        {items.map((item, idx) => (
          <CarouselItem key={idx}>
            <img
              className="respect-height"
              src={item.thumbnail ?? item.url}
              alt={item.alt}
            />
          </CarouselItem>
        ))}
      </Carousel>
    </div>
  );
}
