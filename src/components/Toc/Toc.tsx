import React from 'react';
import type { MarkdownHeading } from 'astro';
// import styles from './Toc.module.css';

interface Props {
  headings: MarkdownHeading[];
}

// TODO: Change this for the floating container with Intersection Observer
// FIXME: Create real nesting and not the ilussion of nesting (aka nested ul and not padding multiplied by depth)
export default function Toc({ headings }: Props): JSX.Element {
  return (
    <ul className="mb-3">
      {headings.map((item, idx) => (
        <li
          key={idx}
          style={{ paddingLeft: item.depth > 1 ? 20 * item.depth : 0 }}
        >
          <a href={`#${item.slug}`}>
            {item.depth} - {item.text}
          </a>
        </li>
      ))}
    </ul>
  );
}
