import React from 'react';

import classes from './Carousel.module.css';

interface Props {
  children: JSX.Element | JSX.Element[];
}

export default function CarouselItem({ children }: Props): JSX.Element {
  return (
    <div className={classes.item}>
      <div className={classes.itemContent}>{children}</div>
    </div>
  );
}
