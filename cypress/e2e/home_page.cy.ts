describe('The Home Page', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Successfully load', () => {
    cy.get('h1')
      .should('have.length', 1)
      .should('have.text', 'Alexander Navarro');
  });

  it('Card components have content', () => {
    cy.get('.card')
      .should('have.length', 3)
      .each(($card) => {
        cy.wrap($card)
          .find('li')
          .should(($li) => expect($li).to.have.lengthOf.at.most(3))
          .find('a')
          .should('have.attr', 'href');

        cy.wrap($card).contains('See more...');
      });
  });
});
