# Personal Webpage

This is a static site created for personal information and content serving such
as blog and others.

## Dependencies

One of the objective of this project is to use as few dependencies as possible
(the ones used are intended for easier and faster development), and try to do
everything from scratch.

Here is the list of main code dependencies:

- Astro Website framework.
- ReactJS for interactive components.
- Sass for stylesheets generations.
- [Gardevoir](https://github.com/krshoss/gardevoir) for CSS reset and normalization.
- [astro-i18next](https://github.com/yassinedoghri/astro-i18next) for translations.

And here is a list of color schemes used for this project:

- [Catppuccin](https://github.com/catppuccin/catppuccin)

## Reference

Cool websites for inspiration:

- [Noel Berry](https://noelberry.ca/)
- [Heidy Motta](https://www.heidy.page/)
- [Zea Slosar](https://zeaslosar.com/)
- [Harrison Gibbins](https://harrisongibbins.com)
- [The Messenger Game](https://themessengergame.com/)
- [Sabotage Studio](https://sabotagestudio.com/)
- [Eva Decker](https://eva.town/)
- [Cory Hughart](https://coryhughart.com/)
- [Brad Frost](https://bradfrost.com/)
- [dvlpr](https://dvlpr.pro/#home)
