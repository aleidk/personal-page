## 0.3.0 (2024-03-15)

### Feat

- **Style**: change layout to allow navbar to expand the whole width
- **Style**: add animations and effects to landing page
- **Navbar**: apply sticky style to the main navbar

### Refactor

- **Components**: move offcanvas from navbar to it's own component

## 0.2.0 (2024-03-13)

### Feat

- **Style**: Apply responsive design

### Refactor

- **Style**: add SASS to reduce style repetition

## 0.1.0 (2024-03-13)

### Feat

- Apply some elevation to landing components
- Apply skeleton of new design to landing page
- **Layout**: add loading spinner component and to layout
- **Layout**: apply view transition to page navigation
- **components**: add carousel components
- **components**: Add lightgallery library

### Fix

- **woodpecker**: incorrect yaml syntax

### Refactor

- move submodule into this repo
